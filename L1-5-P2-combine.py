from Cimpl import *

def combine(red_image: Image, green_image: Image, blue_image:Image)-> Image:
    """Author: Christopher Semaan
    Returns combined image from three inputed images
    
    """
    combined_image = create_image(get_width(red_image), get_height(red_image))
    
    for pixel in combined_image:
        x, y, (r, g, b) = pixel 
        color1 = get_color(red_image, x, y)
        r = color1[0]
        color2 = get_color(green_image, x, y)
        g = color2[1]
        color3 = get_color(blue_image, x, y)
        b = color3[2]
        
        combined_color =  create_color(r, g, b)
        set_color(combined_image, x, y, combined_color)
            
    return combined_image

red = load_image('red_image.jpg')
blue = load_image('blue_image.jpg')
green = load_image('green_image.jpg')
test_image = combine(red, green, blue)
show(test_image)

def test_combine():
    """Author: Christopher Semaan
    Returns True if the r, g, b values of the red, green, blue filters match with the the r, g, b of the combined filter
    """
    test_pass = 0
    for pixel in test_image:
        x, y, (r, g, b) = pixel     
        if get_color(red, x, y)[0] == get_color(test_image, x, y)[0] and get_color(green, x, y)[1] == get_color(test_image, x, y)[1] and get_color(blue, x, y)[2] == get_color(test_image, x, y)[2]:
            test_pass = True
        else:
            test_pass = False
    return test_pass