from Cimpl import load_image, choose_file, get_height, get_width, copy, \
     get_color, create_color, set_color, Image, show, create_image, save_as
from L1_5_image_filters import extreme_contrast, _adjust_component, \
     posterize, grayscale, sepia, three_tone, two_tone, detect_edges\
     , flip_horizontal, flip_vertical, detect_edges_better


"""Milestone 3, L1-5, 1/12/2019
Christopher Semaan, Hannah Samson, Dana El Sherif, Omar Yehia
"""

#print function
def print_prompt()->str:
    """Christopher Semaan
    Hannah Samson
    Dana El Sherif
    Omar Yehia 
    
    Returns input changed to uppercase
    
    L)oad image  S)ave-as
    2)-tone  3)-tone  X)treme contrast  T)int sepia  P)osterize
    E)dge detect  I)mproved edge detect  V)ertical flip  H)orizontal flip
    Q)uit 
    
    : 
    """
    print("L)oad image  S)ave-as")
    print("2)-tone  3)-tone  X)treme contrast  T)int sepia  P)osterize")
    print("E)dge detect  I)mproved edge detect  V)ertical flip  H)orizontal flip")
    print("Q)uit \n")
    filter_input = str.upper(input(": "))
    
    return filter_input

valid_commands = ["L", "S", "2", "3", "X", "T", "P", "E", "I", "V", "H", "Q"]  

#input function
def check_input(filter_input: str)->Image:
    """Christopher Semaan
    Hannah Samson
    Dana El Sherif
    Omar Yehia 
    Returns cumulatively filtered image based on input of which filters selected
    
    L)oad image  S)ave-as
    2)-tone  3)-tone  X)treme contrast  T)int sepia  P)osterize
    E)dge detect  I)mproved edge detect  V)ertical flip  H)orizontal flip
    Q)uit 
    
    : 
    """
    image_selected = False
    
    if filter_input == "Q":
        image=0
        print("No image loaded")
        filter_input = print_prompt()
        
    while filter_input != "Q":
        
        if filter_input in valid_commands:
            
            if filter_input == "L":
                image = load_image(choose_file())
                image_selected = True
                show(image)
                
            elif filter_input == "S" and image_selected:
                save_as(image, 'final_image.jpg')
                
            elif filter_input == "2" and image_selected:
                image = two_tone(image, "yellow", "cyan")
                show(image)
                
            elif filter_input == "3" and image_selected:
                image = three_tone(image, "yellow", "magenta", "cyan")
                show(image)
                
            elif filter_input == "X" and image_selected:
                image = extreme_contrast(image)
                show(image)
                
            elif filter_input == "T" and image_selected:
                image = sepia(image)
                show(image)
                
            elif filter_input == "P" and image_selected:
                image = posterize(image)
                show(image)
                
            elif filter_input == "E" and image_selected:
                threshold = input('Threshold?: ')
                image = detect_edges(image, int(threshold))
                show(image)
                
            elif filter_input == "I" and image_selected:
                threshold = input('Threshold?: ')
                image = detect_edges_better(image, int(threshold)) 
                show(image)
                
            elif filter_input == "V" and image_selected:
                image = flip_vertical(image)
                show(image)
                
            elif filter_input == "H" and image_selected:
                image = flip_horizontal(image)
                show(image)
                
            elif image_selected != True:
                print("No image loaded")
                
            filter_input = print_prompt()
        else:
            print("No such command")
            filter_input = print_prompt()
    return image
        
filter_input = print_prompt()
check_input(filter_input)