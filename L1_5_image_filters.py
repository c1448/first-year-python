from Cimpl import load_image, choose_file, get_height, get_width, copy, \
     get_color, create_color, set_color, Image, show

#Milestone 2, L1-5, 24/11/2019

def extreme_contrast(image:Image) -> Image:
    """
    Author: Hannah Samson
    
    >>> extreme_contrast()
    
    Return a filtered image of the input with extreme contrast.
    
    """
    
    new_image = copy(image)
    
    for pixel in new_image:
        x,y, (r, g, b) = pixel
        if 0 <= r <= 127: 
            set_color(new_image, x, y, create_color(0, g, b))
        else:
            set_color(new_image, x, y, create_color(255, g, b))
        
            
    for pixel in new_image:
        x,y, (r,g,b) = pixel
        if 0 <= g <= 127:
            set_color(new_image, x, y, create_color(r, 0, b))
        else:
            set_color(new_image, x, y, create_color(r, 255, b))
                
    for pixel in new_image:
        x,y, (r,g,b) = pixel
        if 0 <= b <= 127:
            set_color(new_image, x, y, create_color(r, g, 0))
        else: 
            set_color(new_image, x, y, create_color(r, g, 255))
             
    return new_image
        

def  _adjust_component(pixel_value: int) -> int:
    """ Author: Omar Yehia
    
    Identifies which quadrant the component is in.
    
    >>> _adjust_component(71)
    >>> 2
    """
    
    if 0 <= pixel_value <= 63:
        return 1
    elif 64 <= pixel_value <= 127:
        return 2
    elif 128 <= pixel_value <= 191:
        return 3
    else:
        return 4
        
def posterize(copy_image: Image) -> Image:
    """ Author: Omar Yehia
    
    Returns a new image with the posterize filter applied.
    
     Please type image filename: riveter.jpg
    >>> posterize(Image)
            Thank you for using my filter!
    """
    copy_image = copy(copy_image)
    
    for x,y,(r, g, b) in copy_image:
        
        redin1 = create_color(31, g, b)
        redin2 = create_color(95, g, b)
        redin3 = create_color(159, g, b)
        redin4 = create_color(223, g, b)        
        
        if _adjust_component(r) == 1:
            set_color(copy_image, x, y, redin1)
            
        elif _adjust_component(r) == 2:
            set_color(copy_image, x, y, redin2)
            
        elif _adjust_component(r) == 3:
            set_color(copy_image, x, y, redin3)  
            
        else:
            set_color(copy_image, x, y, redin4) 
    
    for x,y,(r,g,b) in copy_image:
        
        greenin1 = create_color(r, 31, b)
        greenin2 = create_color(r, 95, b)
        greenin3 = create_color(r, 159, b)
        greenin4 = create_color(r, 223, b)        
        
        if _adjust_component(g) == 1:
            set_color(copy_image, x, y, greenin1)
            
        elif _adjust_component(g) == 2:
            set_color(copy_image, x, y, greenin2)
            
        elif _adjust_component(g) == 3:
            set_color(copy_image, x, y, greenin3) 
            
        else:
            set_color(copy_image, x, y, greenin4)
    
    for x,y,(r,g,b) in copy_image:
        
        bluein1 = create_color(r, g, 31)
        bluein2 = create_color(r, g, 95)
        bluein3 = create_color(r, g, 159)
        bluein4 = create_color(r, g, 223)           
        
        if _adjust_component(b) == 1:
            set_color(copy_image, x, y, bluein1)
            
        elif _adjust_component(b) == 2:
            set_color(copy_image, x, y, bluein2)
            
        elif _adjust_component(b) == 3:
            set_color(copy_image, x, y, bluein3)  
            
        else:
            set_color(copy_image, x, y, bluein4)
    
    posterized_image = copy_image   
    print('Thank you for using my filter!')
    return posterized_image


def grayscale(image: Image) -> Image:
    """Return a grayscale copy of image.
   
    >>> image = load_image(choose_file())
    >>> gray_image = grayscale(image)
    >>> show(gray_image)
    """
    new_image = copy(image)
    for x, y, (r, g, b) in image:
        
        brightness = (r + g + b) // 3
        
        gray = create_color(brightness, brightness, brightness)
        set_color(new_image, x, y, gray)      
    return new_image



def sepia(image: Image) -> Image:
    """Dana El Sherif
    Returns new image with sepia filter applied (changing r and b values)
    
    >>>image = load_image(choose_file())
    sepia(image)
    
    """
    sepia_image = grayscale(copy(image))
    
    
    for x,y, (r, g, b) in sepia_image:
        if r < 63:        
            newred = r * 1.1
            newblue = b * 0.9
            
        elif r < 192:
            newred = r * 1.15
            newblue = b * 0.85
            
        else:
            newblue = b * 0.93
            newred = r * 1.08
            
        sepia_color = create_color(newred, g, newblue)
        set_color(sepia_image, x, y, sepia_color)
            
    return sepia_image            


def three_tone(image: Image, colour1: "str", colour2: "str", colour3: "str") -> Image:
    """ Christopher Semaan
    Returns filtered image from input
    >>>three_tone(image, "black", "white", "yellow")
    
    """
    
    new_image = copy(image)
    colours= [colour1, colour2, colour3]
    rgb_list = []
    
    for i in range(len(colours)):
        if colours[i] == "black":
            rgb_list+= (0, 0, 0)
            
        elif colours[i] == "white":
            rgb_list += (255, 255, 255)
            
        elif colours[i] == "red":
            rgb_list+= (255, 0, 0)
            
        elif colours[i] == "lime":
            rgb_list+= (0, 255, 0)
            
        elif colours[i] == "blue" :
            rgb_list += (0, 0, 255)
            
        elif colours[i] == "yellow":
            rgb_list += (255, 255, 0)
            
        elif colours[i] == "cyan":
            rgb_list += (0, 255, 255)
            
        elif colours[i] == "magenta":
            rgb_list += (255, 0, 255)
            
        elif colours[i] == "gray":
            rgb_list += (128, 128, 128)
            
    colour1 = create_color(rgb_list[0], rgb_list[1], rgb_list[2])
    colour2 = create_color(rgb_list[3], rgb_list[4], rgb_list[5]) 
    colour3 = create_color(rgb_list[6], rgb_list[7], rgb_list[8])
    
    for pixel in image:
        x, y, (r ,g, b) = pixel
        avg = (r + g +b) / 3 
        if avg < 84:
            set_color(new_image, x, y, colour1)
        elif 84<avg<170 : 
            set_color(new_image, x, y, colour2)
        else:
            set_color(new_image, x, y, colour3)
            
        
         
    return new_image    


def two_tone(image:Image, colour1:str, colour2:str)->Image:
    """Christopher Semaan
    Takes input image and returns filtered image that changes r,g,b values of each pixel
    >>>three_tone(image, "black", "white")
    
    """
    new_image = copy(image)
    colours= [colour1, colour2]     # list for inputed strings
    rgb_list = [] #new list for RGB values
    
    for i in range(len(colours)):
        if colours[i] == "black":
            rgb_list+= (0, 0, 0)
            
        elif colours[i] == "white":
            rgb_list += (255, 255, 255)
            
        elif colours[i] == "red":
            rgb_list+= (255, 0, 0)
            
        elif colours[i] == "lime":
            rgb_list+= (0, 255, 0)
            
        elif colours[i] == "blue" :
            rgb_list += (0, 0, 255)
            
        elif colours[i] == "yellow":
            rgb_list += (255, 255, 0)
            
        elif colours[i] == "cyan":
            rgb_list += (0, 255, 255)
            
        elif colours[i] == "magenta":
            rgb_list += (255, 0, 255)
            
        elif colours[i] == "gray":
            rgb_list += (128, 128, 128)
            
    colour1 = create_color(rgb_list[0], rgb_list[1], rgb_list[2])
    colour2 = create_color(rgb_list[3], rgb_list[4], rgb_list[5]) 
        
    for pixel in image:
        x, y, (r,g,b) = pixel
        avg = (r + g +b) / 3 
        if avg < 128:
            set_color(new_image, x, y, colour1)
        else: 
            set_color(new_image, x, y, colour2)
        
    
         
    return new_image


def detect_edges(image: Image, threshold: int)->Image:
    """Christopher Semaan
    Returns image that changes RGB values to either white or black based on input threshold
    
    >>>detect_edges(image, 5)
    """
    image = copy(image)
    
    for y in range(get_height(image) -1):
        for x in range(get_width(image)):
            
            
            top_row_colour = get_color(image, x , y)
            bottom_row_colour = get_color(image, x, y + 1)
            
            
            brightness_top =  (top_row_colour[0] + top_row_colour[1] \
                               + top_row_colour[2])/3
            brightness_bottom = (bottom_row_colour[0] + bottom_row_colour[1] \
                                + bottom_row_colour[2]) /3
            
            if(abs(brightness_top - brightness_bottom) > threshold):
                top_row_colour = create_color(0, 0 ,0)
                set_color(image, x, y, top_row_colour)  
            else:
                top_row_colour = create_color(255, 255, 255)
                set_color(image, x, y, top_row_colour)  
          
        
     
    return image


def flip_horizontal(image: Image) -> Image:
    """ Omar Yehia
    
    Returns an image after it has been flipped horizontally.
    
    >>> flip_horizontal(original_image)
    """
    
    height = get_height(image)
    width = get_width(image)
    new_image = copy(image)
    
    y2 = height - 1
    
    for y in range(0, height //2):
        x2 = 0
        for x in range(0, width):
            pxl = get_color(image, x, y)
            r, g, b = pxl
            pxl_col = create_color(r, g, b)
            set_color(new_image, x2, y2, pxl_col)
            
            pxl2 = get_color(image, x2, y2)
            r, g, b = pxl2
            pxl2_col = create_color(r, g, b)
            set_color(new_image, x, y, pxl2_col)
            
            x2 += 1
        y2 -= 1
        
     
    return new_image

def flip_vertical(image: Image) -> Image:
    """Author: Dana El Sherif
    Returns an image after it has been flipped vertically.
    
    >>> flip_vertical(original_image)
    """
    
    height = get_height(image)
    width = get_width(image)
    flipped_image = copy(image)
    
    x2 = width - 1
    
    for x in range(0, width //2):
        y2 = 0
        for y in range(0, height):
            picture = get_color(image, x, y)
            r, g, b = picture
            colour = create_color(r, g, b)
            set_color(flipped_image, x2, y2, colour)
            
            picture2 = get_color(image, x2, y2)
            r, g, b = picture2
            colour2 = create_color(r, g, b)
            set_color(flipped_image, x, y, colour2)
            
            y2 += 1
        x2 -= 1
        
     
    return flipped_image
   

def detect_edges_better(image:Image, threshold: float)-> Image:
    """Hannah Samson
    
    This filter takes the input image and returns a filtered image that changes 
    r,g,b values of each pixel to black or white, depending on whether the 
    difference in the brightness of the pixels above, below and beside each 
    other are greater than the threshold.
    
    """
    height = get_height(image)
    width= get_width(image)
    
    for x in range(width - 1): 
        for y in range(height - 1):       
            r, g, b = get_color(image, x, y)           
            black = create_color(0,0,0)
            white = create_color(255,255,255)
            
            r, g, b = get_color(image, x, y) 
            r1, g1, b1 = get_color(image, x, y + 1)  
            r2, g2, b2 = get_color(image, x + 1, y)
            
            brightness_top = round(( r + g + b ) // 3, 2)
            brightness_bottom = round( ( r1 + g1 + b1 ) // 3, 2 )
            brightness_beside = round ( ( r2 + g2 + b2 ) // 3, 2)
                    
            contrast1 = abs(brightness_top - brightness_bottom)
            contrast2 = abs(brightness_top - brightness_beside)
                
            if contrast1 > threshold or contrast2 > threshold:
                set_color(image,x,y,black)
                
            else: set_color(image,x,y,white)                                
    return image   


def red_channel(image: Image) -> Image:
    
    """Return a red copy of image. Each pixel's component provides
    the RGB components for the corresponding red shade.
    -Dana El Sherif
    
    INPUT SHOULD BE FILE NAME
    
    >>> filename = choose_file()
    >>> image = load_image(filename) 
    >>> red_image = red_channel(image)
    >>> show(red_image)     
    """
    image = load_image(image)
    red_image = copy(image)
    for x,y, (r,g,b) in image:
        #x,y, colour = pixel
        #r,g,b = colour
        red = create_color(r,0,0)
        set_color(red_image, x, y, red)
        
    show (red_image)
    save_as(red_image, 'red_image.jpg')
    return red_image
    
def green_channel() -> Image:
    """ Hannah Samson
       
    Return a green copy of the image. Each pixel's component provides the RGB
    components for the corresponding green shade.
    
    >>>green_channel()
    """
    image = load_image(input('Input file name \n')) #reads the image
       
    for x,y, (r, g, b) in image: #reads through the image pixel by pixel
        green= create_color(0, g, 0) #sets color
        set_color(image, x, y, green) #sets the pixels of that colour to the defined locations
       
    show(image)
    return image


#Blue filter and test
def blue_channel() -> Image: 
    """ Author: Omar Yehia
    Returns a blue copy of the image
    
    CALL FUNCTION THEN INPUT FILE NAME
    """
    image = load_image(input('Please enter image filename: \n'))
    new_image = copy(image)
    
    
    for x, y, (r, g, b) in image:
        blue = create_color(0, 0, b)
        set_color(new_image, x, y, blue)
    show(new_image)
    return new_image