CONTACT INFORMATION
—————————————————
christopherjsemaan@cmail.carleton.ca

DATE
—————————————————
December 1st, 2019

SOFTWARE INFORMATION
—————————————————
COHD Photo Editing (R)  Version 1.0.0 1/12/2019


DESCRIPTION
—————————————————
COHD is a photo editing application that applies one of nine filters that are preset. 
Online price: $19.99 (excl. tax)
Retail price: $24.99 (excl. tax)



GENERAL USAGE NOTES
—————————————————
- User is prompt with possible options

L)had image   S)ave-as
2)-tone   3) -tone   X)treme contrast   T)int sepia   P)overtime
E)dge detect   I)mproved edge detect   V)ertical flip   H)orizontal flip
Q)uit


- User should load an image by pressing L
- User should pick a filter by pressing the corresponding key in the possible options
- After image shows, user has the option to save the image by pressing S


SYSTEM RECOMMENDATIONS
—————————————————
Operating system: Windows XP or higher, Mac OS X or higher
RAM: 4GB
Storage: 250KB
CPU: 1.6GHz


INSTALLATION
—————————————————
- Download L1_5_image_filters.py and L1_5_interactive_ui.py and place them in the same
directory.
- Recommended IDE to run python is Wing101. Available on Windows, macOS and Linux.


CREDITS
—————————————————
Christopher Semaan - 2/3 tone, edge detection
Hannah Samson - extreme contrast, improved edge detection
Dana El Sherif - sepia, vertical flip
Omar Yehia - posterize, horizontal flip


LICENSE
—————————————————
Copyright 2013-2017, Cimpl, D.L. Bailey. Department of Systems and Computer Engineering. Carleton University.

Copyright (c) 2019 COHD Photo Editing. L1-5 Engineering Inc., Carleton University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.



